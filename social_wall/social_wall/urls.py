from django.contrib import admin
from django.conf.urls import url, include
from feed import views, urls
from rest_framework.routers import DefaultRouter

# Create a router and register our viewsets with it.
router = DefaultRouter(schema_title='Social Wall API')
router.register(r'social_networks', views.SocialNetworkViewSet)
router.register(r'posts', views.PostViewSet)
router.register(r'recentposts', views.RecentPostViewSet)
router.register(r'hiddenposts', views.HiddenPostViewSet)
router.register(r'feed', views.FeedViewSet)
# router.register(r'feed', views.FeedListView)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^feed', views.FeedListView.as_view()),
    url(r'^hidden_feed', views.HiddenFeedListView.as_view()),
    url(r'^', views.FeedListView.as_view()),
]