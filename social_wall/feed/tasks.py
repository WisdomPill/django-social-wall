from __future__ import absolute_import
from datetime import timedelta
from dateutil.parser import parse
from celery import shared_task
from celery.task import periodic_task
from celery.utils.log import get_task_logger
from feed.models import Post, SocialNetwork, Keyword
from TwitterSearch import *

import json

log = get_task_logger(__name__)

# it's about time to create a TwitterSearch object with our secret tokens
ts = TwitterSearch(
    consumer_key = 'XTyohXpPYJEFWUV0nOAmxYPTk',
    consumer_secret = 'NSDQUm83AqtIqVrCEbLQM5Jg35TFcpr6iBHqLcxiML6IBEAxp2',
    access_token = '68462666-QEh9kOjJLPw1LXZMFcVLCIylz91TBmnEJXmLWqq0K',
    access_token_secret = 'pBuIcw97focuH7E0rLfxhUQgMyCPzGfQ2BeAR1UX4zdb4'
)

# @shared_task
# def add(x, y):
#     log.info('Adding {0} + {1}'.format(x, y))
#     return x + y

# @periodic_task(run_every=timedelta(seconds=10))
# def hello():
#     log.info('it works')
#     # log.debug('log.debuging random')
#     return 'Hello World'

@periodic_task(run_every=timedelta(seconds=10))
def get_tweets():
    try:
        twitter = SocialNetwork.objects.get(name='twitter')
        keywords = [k.content for k in Keyword.objects.filter(social_network=twitter)]

        log.debug("I'm looking for {}".format(keywords))
        tso = TwitterSearchOrder()
        tso.set_include_entities(True) # for getting photos

        tso.set_keywords(keywords, or_operator=True)

        last_id = twitter.last_id
        # log.warning("type -> {}".format(type(twitter)))
        log.debug("twitter -> {}".format(twitter))

        log.debug("last_seen -> {}".format(twitter.last_seen))

        # log.warning("last_seen -> {}".format(twitter.get_last_seen()))
        if last_id is not None and last_id != 0:
            tso.set_since_id(last_id)
        else:
            last_id = 0

        tweets = ts.search_tweets_iterable(tso)

        log.debug("last_id -> {}".format(last_id))
        log.debug("found {} tweets :)".format(tweets.get_amount_of_tweets()))

        # this is where the fun actually starts :)
        for tweet in tweets:
            photo = None
            # tweet_data = {}
            if tweet.get('entities'):
                log.debug("There's some entities")
                if tweet['entities'].get('media'):
                    log.debug("There's some media")
                    for a in tweet['entities']['media']:
                        log.debug(a)
                        photo = a['media_url']
                    log.debug("found photo at {}".format(photo))
            # try:
            #     log.debug(tweet['entities'])
            #     photo = tweet['entities']['media'][0]['url']
            #     log.debug("found photo at {}".format(photo))
            # except:
            #     log.debug("No photos found")
            new_tweet = Post(user_name=tweet['user']['name'], user_photo = tweet['user']['profile_image_url'], user_handle = tweet['user']['screen_name'],
                            content = tweet['text'], pub_date = parse(tweet['created_at']), social_network = twitter,
                            photo = photo, hidden = 0)
            new_tweet.save()

            last_id = max(last_id, tweet['id'])

            # try:
            #     log.debug("Saved a new tweet -> {}".format(tweet['entities']))
            # except:
            #     log.debug("No entities")
            # log.debug( '@%s tweeted: %s' % ( tweet['user']['screen_name'], tweet['text'] ) )

        twitter.last_id = last_id
        twitter.save()

    except TwitterSearchException as e: # take care of all those ugly errors if there are some
        log.error(e)