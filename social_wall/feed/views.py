from django.views.generic import ListView
from feed.models import Post, SocialNetwork
from feed.serializers import PostSerializer, SocialNetworkSerializer
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import viewsets

# import the logging library
import logging

# Get an instance of a logger
log = logging.getLogger(__name__)


class PostViewSet(viewsets.ModelViewSet):
    '''
    ViewSet that displays all the posts, hidden posts included
    '''
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class FeedViewSet(viewsets.ModelViewSet):
    '''
    ViewSet that displays all the posts, hidden posts included
    '''
    queryset = Post.objects.filter(hidden=False).order_by('-pub_date')[:100]
    serializer_class = PostSerializer

class RecentPostViewSet(viewsets.ReadOnlyModelViewSet):
    '''
    ViewSet that displays recent posts after a post id

    It needs a JSON file like the following
    {
        "last_id" : int
    }
    '''
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    parser_classes = (JSONParser,)

    def list(self, request):

        recent_feed = Post.objects.filter(hidden=False).order_by('-pub_date').filter(pk__gt=request.query_params.get('last_id'))

        log.warning("incoming request")
        log.warning(dir(request))
        # log.warning(request.data)
        log.warning(request.query_params)

        serializer = self.get_serializer(recent_feed, many=True)
        return Response(serializer.data)


class HiddenPostViewSet(viewsets.ReadOnlyModelViewSet):
    '''
    ViewSet that displays hidden posts
    '''
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    parser_classes = (JSONParser,)

    def list(self, request):

        recent_feed = Post.objects.filter(hidden=True)

        log.warning("incoming request")
        log.warning(dir(request))
        # log.warning(request.data)
        log.warning(request.query_params)

        serializer = self.get_serializer(recent_feed, many=True)
        return Response(serializer.data)


class SocialNetworkViewSet(viewsets.ReadOnlyModelViewSet):
    '''
    ViewSet that displays all the social networks
    '''
    queryset = SocialNetwork.objects.all()
    serializer_class = SocialNetworkSerializer


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'posts': reverse('post-list', request=request, format=format),
        'recentposts': reverse('recent-post-list', request=request, format=format),
        'social_networks': reverse('social_networks-list', request=request, format=format)
    })

class HiddenFeedListView(ListView):
    model = Post
    queryset = Post.objects.filter(hidden=True).order_by('-pub_date')[:100]
    template_name = 'feed/feed.html'

    def get_context_data(self, **kwargs):
        ctx = super(HiddenFeedListView, self).get_context_data(**kwargs)
        ctx['only_hidden'] = True
        return ctx


class FeedListView(ListView):
    model = Post
    queryset = Post.objects.filter(hidden=False).order_by('-pub_date')[:100]
    template_name = 'feed/feed.html'
    only_hidden = None

    def get_context_data(self, **kwargs):
        ctx = super(FeedListView, self).get_context_data(**kwargs)
        ctx['only_hidden'] = False
        return ctx