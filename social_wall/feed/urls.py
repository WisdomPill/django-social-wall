from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

recent_post_list = views.RecentPostViewSet.as_view({
    'get': 'list',
})

hidden_post_list = views.HiddenPostViewSet.as_view(
{
    'get' : 'list'
})

feed_list = views.FeedViewSet.as_view({
    'get' : 'retrieve'
})

post_list = views.PostViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

post_detail = views.PostViewSet.as_view({
    'get': 'retrieve',
    # 'put': 'update',
    'patch': 'partial_update',
    # 'delete': 'destroy'
})

social_network_list = views.SocialNetworkViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

social_network_detail = views.SocialNetworkViewSet.as_view({
    'get': 'retrieve'
})

# API endpoints
urlpatterns = [
    # url(r'^feed/$', feed_list, name='feed'),
    url(r'^$', views.api_root, name='api-root'),
    url(r'^recentposts/$', recent_post_list, name='recent-post-list'),
    url(r'^hiddenposts/$', hidden_post_list, name='hidden-post-list'),
    url(r'^feed/$', feed_list, name='feed-list'),
    url(r'^posts/$', post_list, name='post-list'),
    url(r'^posts/(?P<pk>[0-9]+)/$', post_detail, name='post-detail'),
    url(r'^social_network/$', social_network_list, name='social_network-list'),
    url(r'^social_network/(?P<pk>[0-9]+)/$', social_network_detail, name='social_network-detail'),
]