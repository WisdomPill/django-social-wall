from django.contrib import admin
from feed.models import SocialNetwork, Post, Keyword

admin.site.register(SocialNetwork)
admin.site.register(Post)
admin.site.register(Keyword)