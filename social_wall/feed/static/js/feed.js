var interval = null;

var csrftoken = $.cookie('csrftoken');

function csrfSafeMethod(method)
{
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup(
{
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$(document).ready(
    function()
    {
        set_handlers();
        interval = setInterval(get_feed_updates, 2000);
    }
);

function set_handlers()
{
    var json_parameter = hide == "False";
    console.log(json_parameter);
    $('.card').each(
        function()
        {
            $(this).click(
                function()
                {
                    var id = $(this).find('p.post-id').text().trim();
                    console.log(id);
                    $.ajax(
                    {
                        type: 'PATCH',
                        url: 'http://' + serverip + '/api/posts/' + id + '/',
                        contentType: 'application/json',
                        dataType: 'json',
                        data: JSON.stringify({ hidden : json_parameter }),
                        processData : false,
                        success: function(data){
                            console.log(data);
                        }
                    });
                    $(this).parents(".post").fadeOut().remove();
                    rearrange();
                });
        });
}

function get_last_id()
{
    var post_ids = [];
    $('.card').each(
        function()
        {
            post_ids.push($(this).find('p.post-id').text().trim());
        });
    post_ids.push(0);
    return Math.max.apply(Math, post_ids)
}

function rearrange()
{
    var cards = [];
    $('.post').each(
        function()
        {
            cards.push($(this));
        });
    var updated_feed = '';
    var card_index = 0;
    cards.forEach(
        function(value)
        {
            if(card_index%4==0)
                updated_feed += '<div class="row">';
            updated_feed += '<div class="col-xs-3 post">';
            
            updated_feed += value.html();

            updated_feed += '</div>';

            card_index++;
            if(card_index%4==0)
                updated_feed += '</div>';
        });

    $('.feed').html(updated_feed);
    set_handlers();
}

function get_feed_updates()
{
    var updates_url = hide == "False" ? '/api/feed/' : '/api/hiddenposts/'
    console.log(updates_url);
    $.ajax({
        type: 'GET',
        url: 'http://' + serverip + updates_url,
        contentType: 'application/json',
        dataType: 'json',
        success: function(resp)
            {
                console.log("last-id -> " + get_last_id());
                update_cards(resp);
            }
    });
}

function update_cards(data)
{
    var updated_feed = '';

    var card_index = 0;

    $.each(data,
        function(index, value)
        {
            // console.log(value);
            if(card_index%4==0)
                updated_feed += '<div class="row">';
            updated_feed += '<div class="col-xs-3 post">';
            updated_feed += '<div class="card">';
            updated_feed += '<div class="card-header">';
            updated_feed += '<img src="' + value.user_photo + '" class="rounded" alt="User photo">';
            updated_feed += '@'+value.user_handle;
            updated_feed += '</div>';
            if(value.photo != null)
                updated_feed += '<img class="card-img-top post-image" src="' + value.photo + '" alt="Card image cap">';
            updated_feed += '<div class="card-block">';
            updated_feed += '<p class="card-text">' + value.content + '</p>';
            updated_feed += '<p class="post-id" hidden>' + value.pk + '</p>';
            updated_feed += '<p class="card-text"><small class="text-muted">Last updated ' + value.pub_date + '</small></p>';
            updated_feed += '</div>';
            updated_feed += '</div>';
            updated_feed += '</div>';
            // console.log(value.content);
            // console.log(value.photo);
            // console.log(value.pub_date);
            card_index++;
            if(card_index%4==0)
                updated_feed += '</div>';
            // console.log(value.photo);
        });

    $('.feed').html(updated_feed);

    set_handlers();

    // clearInterval(interval);
}