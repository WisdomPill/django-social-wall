from rest_framework import serializers
from .models import Post, SocialNetwork

class SocialNetworkSerializer(serializers.HyperlinkedModelSerializer):
    post = serializers.HyperlinkedRelatedField(many=True, view_name='post-list', read_only=True)

    class Meta:
        model = SocialNetwork
        # fields = ('pk', 'name', 'last_seen', 'last_id')
        exclude = ('post',)

class PostSerializer(serializers.HyperlinkedModelSerializer):
    # social_network = serializers.HyperlinkedRelatedField(many=True, view_name='social_network-detail', read_only=True)
    
    class Meta:
        model = Post
        fields = ('pk', 'user_name', 'user_photo', 'user_handle' , 'content', 'pub_date', 'photo', 'hidden', 'social_network')