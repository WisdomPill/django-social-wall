from django.db import models

class SocialNetwork(models.Model):
    '''
    Simple description to show
    '''
    name = models.CharField(max_length=30)
    last_seen = models.DateTimeField(null=True)
    last_id = models.BigIntegerField(null=True)

    class Meta:
        ordering = ('last_seen',)

    def __str__(self):
        return self.name


class Keyword(models.Model):
    '''
    '''
    content = models.CharField(max_length=50)
    social_network = models.ForeignKey(SocialNetwork)

    def __str__(self):
        return self.content


class Post(models.Model):
    '''

    '''
    user_name = models.CharField(max_length=50, default="User name")
    user_photo = models.CharField(null=True, max_length=256)
    user_handle = models.CharField(max_length=30, default="user_handle")
    content = models.TextField()
    pub_date = models.DateTimeField()
    last_modified = models.DateTimeField(null=True)
    hidden = models.BooleanField()
    photo = models.CharField(null=True, max_length=256)
    social_network = models.ForeignKey(SocialNetwork)

    class Meta:
        ordering = ('pub_date',)

    def __str__(self):
        return self.content