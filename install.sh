# echo "Installing pip3"

# sudo apt-get install -y python3-pip

# echo "Installing virtualenv"

# sudo apt-get install -y virtualenv

# echo "Changing enviroment to social_wall"

# source bin/activate

echo "Installing and configuring rabbitmq"

sudo apt-get install -y rabbitmq-server
sudo rabbitmqctl add_user social_user tedxtrento # the last parameter is the password
sudo rabbitmqctl add_vhost social_host
sudo rabbitmqctl set_user_tags social_user social_tag
sudo rabbitmqctl set_permissions -p social_host social_user ".*" ".*" ".*"

echo "Installing supervisor"

sudo apt-get install -y supervisor

echo "Adding supervisor configuration for social_wall"

sudo cp conf/social_wall.conf /etc/supervisor/conf.d/social_wall.conf

echo "Creating directories for logs"

sudo mkdir /var/log/social_wall
sudo touch /var/log/social_wall/social_wall-celery.log
sudo touch /var/log/social_wall/social_wall-runserver.log

echo "start supervisor"
sudo service supervisor start

echo "Installing sqlite3 browser"
sudo apt-get install -y sqlite3
